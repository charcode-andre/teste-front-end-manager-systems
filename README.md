# Teste - Front End - Manager Systems

## Inicialização do backend
$ java -jar BackendTesteFrontend-1.0.0.jar

## Instalação
$ cd front-end-paises

$ npm install

## Inicialização da aplicação
$ yarn start

## Acesso
http://localhost:4200