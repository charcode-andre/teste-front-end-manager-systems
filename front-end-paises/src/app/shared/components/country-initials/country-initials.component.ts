import {Component, Input} from '@angular/core';
import {Pais} from '../../models/pais.model';

@Component({
  selector: 'app-country-initials',
  templateUrl: './country-initials.component.html',
  styleUrls: ['./country-initials.component.scss']
})
export class CountryInitialsComponent {
  @Input()
  country: Pais;

  formatInitials(event) {
    this.country.sigla = event.target.value.slice(0, 2).toUpperCase();
  }

}
