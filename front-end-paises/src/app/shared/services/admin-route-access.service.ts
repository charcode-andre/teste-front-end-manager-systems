import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {AuthenticationService} from './authentication.service';
import {ToastrService} from 'ngx-toastr';


@Injectable({ providedIn: 'root' })
export class AdminRouteAccessService implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private toastrService: ToastrService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.authenticationService.getAdmin()) {
      return true;
    }
    this.toastrService.error('Você não possui permissão para realizar esta ação.')
    return false;
  }
}
