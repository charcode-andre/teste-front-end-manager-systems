import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {BehaviorSubject} from 'rxjs';
import {Router} from '@angular/router';
import {AuthenticationModel} from '../models/authentication.model';

@Injectable({providedIn: 'root'})
export class AuthenticationService {
  public currentUser: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient, private router: Router) {
  }

  public getAdmin(): any {
    return 'true' === localStorage.getItem('isAdmin');
  }

  public setAdmin(admin) {
    localStorage.setItem('isAdmin', admin);
  }

  public getToken(): any {
    return localStorage.getItem('token');
  }

  public setToken(token) {
    localStorage.setItem('token', token);
  }

  public getCurrentUser(): any {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  public setCurrentUser(currentUser) {
    localStorage.setItem('currentUser', JSON.stringify(currentUser));
  }

  login(login: string, senha: string) {
    const params = new HttpParams()
      .set('login', login)
      .set('senha', senha);
    return this.http
      .post<any>(`${environment.SERVER_API_URL}/usuario/autenticar/`, {}, {params})
      .pipe(map((res: AuthenticationModel) => {
        this.setAdmin(res.administrador);
        this.setToken(res.token);
        this.setCurrentUser(res);
        this.currentUser.next(res);
        return res;
      }));
  }

  requestNewToken() {
    const params = new HttpParams()
      .set('token', this.getToken());
    return this.http
      .get<any>(`${environment.SERVER_API_URL}/usuario/renovar-ticket/`, {params});
  }

  logout() {
    // remove user from local storage to log user out
    this.setToken(null);
    this.setAdmin(false);
    this.setCurrentUser(null);
    this.currentUser.next(null);
    this.router.navigate(['/login']);
  }

}
