import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {AuthenticationService} from './authentication.service';

@Injectable()
export class CountryService {

  private endpoint = `${environment.SERVER_API_URL}/pais`;
  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService
  ) {
  }

  create(entity: any): Observable<HttpResponse<any>> {
    const params = new HttpParams()
      .set('token', this.authenticationService.getToken());
    return this.http.post<HttpResponse<any>>(`${this.endpoint}/salvar`, entity, {
        observe: 'response', params});
  }

  findAll(): Observable<HttpResponse<any>> {
    const params = new HttpParams()
      .set('token', this.authenticationService.getToken());
    return this.http.get<HttpResponse<any>>(`${this.endpoint}/listar`, {
      observe: 'response', params});
  }

  query(filtro: string): Observable<HttpResponse<any>> {
    const param: any = {token: this.authenticationService.getToken(), filtro};
    return this.http
      .get<HttpResponse<any>>(`${this.endpoint}/pesquisar`, {
        observe: 'response', params: {token: param.token, filtro}});
  }

  delete(id: number): Observable<HttpResponse<any>> {
    const params = new HttpParams()
      .set('token', this.authenticationService.getToken()).set('id', id.toString());
    return this.http.get<HttpResponse<any>>(`${this.endpoint}/excluir`, {
      observe: 'response', params
    });
  }
}
