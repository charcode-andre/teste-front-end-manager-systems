import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
@Injectable()
export class LoaderService {
    isLoading: BehaviorSubject<boolean> = new BehaviorSubject(false);
    show() {
        this.isLoading.next(true);
    }
    hide() {
      // timeout apenas para simular um request real, exibindo o loader
      setTimeout(() => this.isLoading.next(false), 1200);
    }
}
