
export class AuthenticationModel {
  constructor(
    public administrador?: boolean,
    public nome?: string,
    public autenticado?: boolean,
    public login?: string,
    public token?: string
  ) {}
}
