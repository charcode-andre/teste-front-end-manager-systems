export class Pais {
  constructor(
    public id?: number,
    public nome?: string,
    public sigla?: string,
    public gentilico?: string
  ) {}
}
