import {
    trigger,
    transition,
    style,
    query,
    group,
    animateChild,
    animate,
  } from '@angular/animations';


export const slider = trigger('routeAnimations', [
    transition('isRight => *', slideTo('left') ),
    transition('isLeft => *', slideTo('right') )
  ]);

function slideTo(direction) {
  const optional = { optional: true };
  return [
    query(':enter, :leave', [
      style({position: 'fixed', top: 0, [direction]: 0, width: '100%'}),
    ], optional),
    query(':enter', [
      style({ [direction]: '-100%'})
    ]),
    group([
      query(':leave', [
        animate('350ms ease', style({ [direction]: '100%'}))
      ], optional),
      query('.card', style({ opacity: '0.5', position: 'fixed'})),
      query(':enter', [
        animate('350ms ease', style({ [direction]: '0%'}))
      ])
    ]),
    // Necessario para animacoes com elementos Child na pagina
    query(':leave', animateChild()),
    query(':enter', animateChild()),
  ];
}
