import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {appRoutes, AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {CountryComponent} from './country/country.component';
import {HomeComponent} from './layout/home/home.component';
import {MainComponent} from './layout/main.component';
import {CountryService} from './shared/services/country.service';
import {ToastrModule} from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CountryUpdateComponent} from './country/country-update.component';
import {CountryInitialsComponent} from './shared/components/country-initials/country-initials.component';
import { LoaderService } from './shared/services/loader.service';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    HomeComponent,
    CountryComponent,
    CountryUpdateComponent,
    CountryInitialsComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes, { useHash: true }),
    ToastrModule.forRoot(),
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  exports: [
    NgbModule
  ],
  providers: [
    CountryService,
    LoaderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
