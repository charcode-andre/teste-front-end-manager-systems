import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../shared/services/authentication.service';
import {Pais} from '../shared/models/pais.model';
import { RouterOutlet } from '@angular/router';
import { slider } from '../route.animations';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  animations: [
    slider,
  ]
})
export class MainComponent {
  paises: Pais[];
  constructor(
    public authenticationService: AuthenticationService,
  ) { }

  logout() {
    this.authenticationService.logout();
  }
  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }

}
