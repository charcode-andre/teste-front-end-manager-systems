import { Component, OnInit } from '@angular/core';
import {CountryService} from '../shared/services/country.service';
import {Pais} from '../shared/models/pais.model';
import {Router} from '@angular/router';
import {AuthenticationService} from '../shared/services/authentication.service';
import {ToastrService} from 'ngx-toastr';
import {LoaderService} from '../shared/services/loader.service';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss']
})

export class CountryComponent implements OnInit {
  paises: Pais[];
  page = 1;
  pageSize = 5;
  lastSortParam: string;
  public collectionSize = this.paises;

  constructor(
    private authenticationService: AuthenticationService,
    private countryService: CountryService,
    private toastrService: ToastrService,
    private loaderService: LoaderService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.loaderService.show();
    this.authenticationService.requestNewToken().subscribe( () => {
      this.countryService
        .findAll()
        .subscribe(res => {
          this.paises = res.body;
          this.loaderService.hide();
        });
    });
  }

  edit(pais: Pais) {
    this.router
      .navigate(['/home', 'country', 'new', {country: pais.nome}]);
  }

  sort(param) {
    if (param !== this.lastSortParam) {
      this.paises = this.paises.sort( (a: Pais, b: Pais) => a[param] > b[param] ? 1 : -1);
      this.lastSortParam = param;
      return;
    }
    this.paises = this.paises.sort( (a: Pais, b: Pais) => a[param] < b[param] ? 1 : -1);
    this.lastSortParam = null;
  }

  delete(pais) {
    this.loaderService.show();
    if (this.authenticationService.getAdmin()) {
      this.authenticationService.requestNewToken().subscribe( () => {
        this.countryService
          .delete(pais.id)
          .subscribe(res => {
            this.loaderService.hide();
            this.loadData();
          }, err => {
            this.toastrService.warning('Seu token expirou, foi necessário renová-lo, por favor refaça a operação.');
          });
      });
      return;
    }
    this.toastrService.error('Você não possui permissão para realizar esta ação.');
  }
}
