import { Component, OnInit } from '@angular/core';
import {CountryService} from '../shared/services/country.service';
import {Pais} from '../shared/models/pais.model';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../shared/services/authentication.service';
import { ChangeDetectorRef } from '@angular/core';
import { LoaderService } from '../shared/services/loader.service';

@Component({
  selector: 'app-country-update',
  templateUrl: './country-update.component.html',
  styleUrls: ['./country.component.scss']
})
export class CountryUpdateComponent implements OnInit {
  pais: Pais = new Pais();

  constructor(
    private countryService: CountryService,
    private activatedRoute: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private router: Router,
    private cd: ChangeDetectorRef,
    private loaderService: LoaderService,
  ) { }

  ngOnInit() {
    if (this.activatedRoute.snapshot.paramMap.get('country')) {
      this.loadByName();
    }
  }

  loadByName() {
    this.loaderService.show();
    this.authenticationService.requestNewToken().subscribe( () => {
      this.countryService
        .query(this.activatedRoute.snapshot.paramMap.get('country'))
        .subscribe( res => {
          // todo não disponibilizaram uma api de find by id por isso a pesquisa por nome
          this.pais = res.body[0];
          this.loaderService.hide();
        });
    });
  }

  save() {
    this.loaderService.show();
    this.authenticationService.requestNewToken().subscribe( () => {
      this.countryService
        .create(this.pais)
        .subscribe(res => {
          this.loaderService.hide();
          this.router.navigate(['/home', 'country']);
        });
    });
  }
}
