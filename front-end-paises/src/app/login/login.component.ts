import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthenticationService} from '../shared/services/authentication.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import { LoaderService } from '../shared/services/loader.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  login: string;
  senha: string;

  constructor(
    private authenticationService: AuthenticationService,
    private toastr: ToastrService,
    private router: Router,
    private loaderService: LoaderService,
  ) { }

  performLogin() {
    this.loaderService.show();
    this.authenticationService.login(this.login, this.senha)
      .subscribe( res => {
        this.loaderService.hide();
        if (res.autenticado) {
          this.router.navigate(['/home']);
        } else {
          this.toastr.error('Credenciais incorretas!');
        }
    }, error => {
        this.toastr.error('Erro ao autenticar!');
        console.error(error);
        this.loaderService.hide();
    });
  }

}
