import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {LoginComponent} from './login/login.component';
import {UserRouteAccessService} from './shared/services/user-route-access.service';
import {CountryComponent} from './country/country.component';
import {MainComponent} from './layout/main.component';
import {HomeComponent} from './layout/home/home.component';
import {CountryUpdateComponent} from './country/country-update.component';
import {AdminRouteAccessService} from './shared/services/admin-route-access.service';

export const appRoutes: Routes = [
  { path: '', component: LoginComponent, pathMatch: 'full', canActivate: [UserRouteAccessService]},
  { path: 'home', component: MainComponent, canActivate: [UserRouteAccessService], children: [
      { path: '', pathMatch: 'full' , component:  HomeComponent},
      { path: 'country', pathMatch: 'full' , component:  CountryComponent, data: { animation: 'isLeft' }},
      { path: 'country/new', pathMatch: 'full' , component:  CountryUpdateComponent, data: { animation: 'isRight' },
      canActivate: [AdminRouteAccessService]},
    ]},
  { path: 'login', component: LoginComponent},
  // caso contrario redirecionar para erro 404
  // { path: '**', redirectTo: '/' + AppConfig.routes.error404 }
];

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes), RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
