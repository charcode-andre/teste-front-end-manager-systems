# FrontEndPaises

Projeto teste para Management System  - Front-end - Cadastro de Países. 

## Development server
<p>ng-serve" para rodar o projeto no servidor local. </p>
<p>porta local servidor - "http://localhost:4200/"</p>
<p>Para build de produção - ng build</p>


##Para Rodar o Servidor Back-end basta digitar dentro da pasta Raíz:
java -jar BackendTesteFrontend-1.0.0.jar



## Testes unitários

rodar `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Observações:
Como solicitado o teste foi desenvolvido em Angular(versão 8), com Biblioteca Bootstrap se utlizando do ng-bootstrap.


